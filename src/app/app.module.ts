import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TareasComponent } from './components/tareas/tareas.component';
import { NotasComponent } from './components/notas/notas.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';


import { FormsModule } from '@angular/forms';
import { ItemTareaComponent } from './components/item-tarea/item-tarea.component';
import { CardNotaComponent } from './components/card-nota/card-nota.component';

@NgModule({
  declarations: [
    AppComponent,
    TareasComponent,
    NotasComponent,
    NavbarComponent,
    FooterComponent,
    ItemTareaComponent,
    CardNotaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
