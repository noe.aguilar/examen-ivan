import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.css']
})
export class NotasComponent implements OnInit {

  listaNotas : any[] = [];

  nota : any = {
    titulo: "",
    descripcion: ""
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardarNota( ngForm : NgForm ){
    
    if( ngForm.invalid )
      return;
    
    let nota = this.nota;
    this.listaNotas.push( {
      titulo: this.nota.titulo,
      descripcion: this.nota.descripcion,
      fecha: new Date()
    } )

    this.nota.titulo = "";
    this.nota.descripcion = "";
  }

}
