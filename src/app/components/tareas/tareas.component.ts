import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {
  
  listaTareas : any[] = []
  tarea : string = "";

  constructor(  ) { }

  ngOnInit(): void {
  }

  agregarTarea( form : NgForm ) {
    
    if( form.invalid ){
      return;
    }

    this.listaTareas.push( {tarea: form.value.texto, check: false} )
    this.tarea = "";
  }

  borrarTareas() {
    this.listaTareas = []
  }

}
