import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item-tarea',
  templateUrl: './item-tarea.component.html',
  styleUrls: ['./item-tarea.component.css']
})
export class ItemTareaComponent implements OnInit {
    
  @Input() tarea: any;

  constructor() { }

  ngOnInit(): void {
  }

}
