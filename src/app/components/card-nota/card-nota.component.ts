import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-nota',
  templateUrl: './card-nota.component.html',
  styleUrls: ['./card-nota.component.css']
})
export class CardNotaComponent implements OnInit {
  
  @Input() nota: any;
  
  constructor() { }

  ngOnInit(): void {
  }

}
